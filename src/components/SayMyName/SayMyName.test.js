import { SayMyName } from "..";

import {render, screen} from '@testing-library/react';

describe('[Test] Component SayMyName', () => {
    it('name exists in App', () => {
        const nameToFind ='sandy';

        render(<SayMyName />);

        expect(screen.getByText(nameToFind, {exact: false})).toBeInTheDocument;
    });

});