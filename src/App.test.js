import {render, screen} from '@testing-library/react';
import App from './App';

describe('[Test] Component App', () => {
    it('reload word exists in App', () => {
        const textToFind ='reload';

        render(<App />);

        expect(screen.getByText(textToFind, {exact: false})).toBeInTheDocument;
    });

    it('React word exists in App', () => {
        render(<App/>);

        expect(screen.getByText('react', {exact: false})).toBeInTheDocument();

    });
});
